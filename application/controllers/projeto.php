
<?php
defined('BASEPATH') OR exit ('No direct script access allowed');


class Projeto extends CI_Controller{
    public function index(){
        $this->load->view('common_projeto/header');
        $this->load->view('common_projeto/navbar');
        $this->load->view('common_projeto/carrosel');
        $this->load->view('common_projeto/footer');
      
    }

    public function empresa(){

        $this->load->view('common_projeto/header');
        $this->load->view('common_projeto/navbar');
        $this->load->view('common_projeto/introducao');
        $this->load->view('common_projeto/footer');
        
    }

    public function servico(){
      $this->load->view('common_projeto/header');
      $this->load->view('common_projeto/navbar');
      $this->load->view('common_projeto/servico');
      $this->load->view('common_projeto/footer');
      
  }

  public function contato(){
    $this->load->helper('html');
    $this->load->view('common_projeto/header');
    $this->load->view('common_projeto/navbar');
    $this->load->view('common_projeto/local');
    $this->load->view('common_projeto/footer');
    
}

public function avaliacao(){
    $this->load->view('common_projeto/header');
    $this->load->view('common_projeto/navbar');
    $this->load->model('UsuarioModel','model');
    $this->model->criar();
    $this->load->view('usuario/form_cadastro');
    $this->load->view('common_projeto/footer');
}


public function painel(){
    $this->load->view('common_projeto/header');
    $this->load->view('common_projeto/navbar');
    $this->load->view('common_projeto/painel');
    $this->load->view('common_projeto/footer');
}
public function aluguel(){
    
        $this->load->view('common_projeto/header');
        $this->load->view('common_projeto/navbar');
        $this->load->model('PainelModel','model');
        $v['listar'] = $this->model->listar();
        $this->load->view('common_projeto/table_view', $v);
        $this->load->view('common_projeto/footer');

    
}



}

?>          