<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/User.php';//caminho do diretorio aplication em libraries

class UsuarioModel extends CI_Model{
    public function criar(){
        if(sizeof($_POST) == 0) return ;
        //$data = $this->input->post();
       //print_r($data);

         $nome = $this->input->post('nome');
         $email= $this->input->post('email');
         $descricao = $this->input->post('descricao');
         $sobrenome = $this->input->post('sobrenome');


         $user = new User($nome,$sobrenome,$email,$descricao);//Objeto Copia da classe 
        //$user->setTelefone($this->input->post('telefone'));//Para itens opcionais -> operador para executar funcao
        $user->save();
            
    }

    public function lista(){
        $html = '';
        $user = new User();
        // Organiza a lista e depois retorna o resultado
        $data =  $user->getAll();
        //print_r($data);
        $html .= '<table class  = "table">';
        foreach ($data as $row){
            $html .='<tr>';
            $html .='<td>' .$row['nome'].'</td>';
            $html .='<td>' .$row['sobrenome'].'</td>';
            $html .='<td>' .$row['email'].'</td>';
            $html .='<td>' .$row['descricao'].'</td>';
            //$html .='<td>' .$row['telefone'].'</td></tr>';
        }
        $html .= '</table>';
        return $html;

    }
}

 /*para ver se a funcao de cima funciona  imprimir os dados apenas essa classe acessa os dados
        Principio do privilegio minimo
        Classe'matriz para criar objeto' define atributos do objeto
        model trabalha com dados*/
?>

