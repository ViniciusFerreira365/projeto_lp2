<?php $this->load->view('common_projeto/header'); ?>
<div class ="container-fluid">
<h3><p class = "text-xl-justify text-monospace">Quem Somos ?</h3>
            <h5><p class = "text-xl-justify font-weight-normal">A LocalCars é uma Locadadora de Veiculos ficticia
                cujo o objetivo é a apresentação do projeto de modularização e  PHP2+Banco de dados, Linguagem de Programação 2.
                </p></h5>
                <h4><p class = "text-xl-justify font-weight-normal">Um Pouco sobre a Historia da Locação de veiculos no Brasil:</p></h4>
                <h5><p class = "text-xl-justify font-weight-normal">O aluguel de automóveis no Brasil tem uma história recente. Começou na romântica década de 1950, quando os antigos Fusca, Dauphine, DKW, Aero Willis, Simca e Karman Guia circulavam pelas ruas, avenidas e rodovias brasileiras, após a indústria automobilística ter sido impulsionada pelo Plano de Metas de Juscelino Kubitschek.
Os primeiros negócios surgiram na região central de São Paulo, onde alguns empresários de revendas de carros usados começaram a alugar os automóveis como atividade suplementar. Foi justamente um desses revendedores, o empresário Adalberto Camargo, que vislumbrou no aluguel de carros um negócio promissor.
Nos anos de 1970, apareceram as empresas de leasing financeiro, com as quais foi possível alavancar o desenvolvimento das locadoras de veículos, já que elas trouxeram a possibilidade de realizar operações de financiamento a longo prazo. No final dessa década, surge também o carro a álcool, marco da indústria nacional, que simbolizou a resposta brasileira à crise mundial do petróleo. As locadoras passaram a utilizar massivamente a novidade, ajudando a divulgá-la por todo o país.
O aluguel de carros, então, já era uma atividade consolidada no Brasil. Como acontecera no exterior, começaram a chegar as redes internacionais de aluguel de automóveis.
Da mesma forma, as empresas nacionais passaram a formar redes, utilizando o sistema de franquia. A locação de veículos ampliou-se consideravelmente.
</p></h5>
<h6><a href='http://www.blogdaslocadoras.com.br/locadoras-de-carros/saiba-mais-sobre-historia-da-locacao-de-veiculos-brasil-e-os-rumos-que-o-negocio-esta-tomando.html'>Link da matéria</a></h6>
</div>
</div>
<div class = "container-fluid">

<img src="<?php echo base_url('assets/img/alugue.jpg');?>" class= "img-fluid float-left" alt="Foto Propaganda antiga">
<img src="<?php echo base_url('assets/img/vintage.jpg');?>" class= "img-fluid rounded mx-auto d-block" alt="Foto Propaganda antiga">

        
            
</div>


