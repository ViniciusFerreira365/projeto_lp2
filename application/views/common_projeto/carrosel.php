
<?php  $this->load->view('common_projeto/header');
echo "<div id='obj_carousel' class='carousel slide w-50 mx-auto mt-2 view overlay zoom' data-ride='carousel'>

                 <ol class='carousel-indicators'>";
                    for($i = 0; $i < 8; $i++) {
                        if ($i == 0 ) {
                            echo "<li data-target='#obj_carousel' data-slide-to='0' class='active'></li>";
                        }
                        else {
                            echo "<li data-target='#obj_carousel' data-slide-to='$i'></li>";
                        }
                    }
                    echo "</ol>

                    <div class='carousel-inner'>";

                    for($i = 1; $i < 8; $i++) {
                        if ($i == 1 ) {
                            echo "<div class='carousel-item active'>";
                        }
                        else {
                            echo "<div class='carousel-item'>";
                        }
                        echo "<img class='w-100' src='assets/img/carro$i.jpg' alt='Imagem de Carros' />
                        </div>";
                    }
                    echo "</div>

                    <a class='carousel-control-prev' href='#obj_carousel' role='button' data-slide='prev'>
                        <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                        <span class='sr-only'>Anterior</span>
                    </a>
                    <a class='carousel-control-next' href='#obj_carousel' role='button' data-slide='next'>
                        <span class='carousel-control-next-icon' aria-hidden='true'></span>
                        <span class='sr-only'>Próximo</span>
                        </a>
                 </div>";

                 

?>

