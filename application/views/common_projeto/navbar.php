<?php $this->load->view('common_projeto/header');?>
<nav class="navbar navbar-expand-lg navbar-dark default-color">
<h2 class='p-2 m-2 bg-sucess text-white text-center'>LocalCars</h2>

  <a class="navbar-brand"href="<?php echo base_url('projeto') ?>">Inicio</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>


  <div class="collapse navbar-collapse" id="basicExampleNav">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link"href="<?php echo base_url('projeto/empresa') ?>">Quem Somos?</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url('projeto/servico') ?>">Nossos serviços</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">Mais</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?php echo base_url('projeto/contato') ?>">  <i class="fab fa-facebook-f fa-fw blue-text"> Contato e redes Sociais </i></a>
          <a class="dropdown-item" href="<?php echo base_url('projeto/avaliacao')?>"> <i class="far fa-comments blue-text"> Comentarios </i></a>
          <a class="dropdown-item" href="<?php echo base_url('projeto/painel')?>"><i class="fas fa-user-tie blue-text"> Area dos Funcionarios </i></a>
        </div>
      </li>
     
    </ul>
  </div>
</nav>


