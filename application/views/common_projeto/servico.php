<div class = "container-fluid">
<h3 class='text-xl-justify'><p class = "text-justify text-monospace">Conheca nossos Serviços</p> </h3>
                <h5><p class = "text-xl-justify font-weight-normal">A LocalCars preza pela segurança,praticicade e rapidez e todos os alugueis.</p></h5>
                <h4><p class = "text-xl-justify font-weight-normal">Oferecemos serviços de quailidade e alta credibilidade, nossos carros sao divididos em categorias para atenter a ampla gama de clientes ofereçendo custo/beneficio</p></h4>
                <h4><p class = "text-xl-justify font-italic">Veja na tabela abaixo todos nossos serviços e disbonibilidade:</p></h4>
</div>
</div>

<div class="table-responsive text-nowrap">
  <table class="table">
    <thead>
      <tr>
        <th scope="col"><i class="fas fa-car-alt"></i> </th>
        <th scope="col">Categora A </th>
        <th scope="col">Categora B </th>
        <th scope="col">Categora C</th>
        <th scope="col">Categora D</th>
        <th scope="col">Categora E</th>
    
      
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Grupo ideal para dia-a-dia</td>
        <td>Grupo ideal para conforto em viagens prolongadas</td>
        <td> Grupo para a Família</td>
        <td>Grupo com luxo e conforto</td>
        <td>Grupo para grandes cargas </td>
   
        
      </tr>
    
      <tr>
        <th scope="row ">2</th>
        <td>Manual</td>
        <td>Manual,Automático</td>
        <td>Manual,Automático,Dupla Embreagem</td>
        <td>Manual,Automático,CVT</td>
        <td>Manual,Automático,CVT</td>
     
      
      </tr>
      <tr>
        <th scope="row">3</th>
        <td>Seguro e travas</td>
        <td>Seguro,travas e Vidros Elétricos</td>
        <td>Seguro,travas,Vidros Elétricos e Direcao Elétrica</td>
        <td>Seguro,travas,Vidros Elétricos,Direcao Elétrica,Blindado(Opcional)</td>
        <td>Seguro,travas,Vidros Elétricos e Direcao Elétrica</td>
     
        
      </tr>

      <tr>
        <th scope="row">4</th>
        <td>Preço de 60 á 85R$/dia</td>
        <td>Preço de 60 á 85R$/dia</td>
        <td>Preço de 60 á 85R$/dia</td>
        <td>Preço de 60 á 85R$/dia</td>
        <td>Preço de 60 á 85R$/dia</td>
       
        
      </tr>
    </tbody>
  </table>
      
    <div class="accordion md-accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
      <div class="card">
        <div class="card-header" role="tab" id="headingOne1">
          <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne1" aria-expanded="true"
            aria-controls="collapseOne1">
            <h5 class="mb-0">
              Grupo A  <i class="fas fa-angle-down rotate-icon"></i>
            </h5>
          </a>
        </div>
        <div id="collapseOne1" class="collapse show" role="tabpanel" aria-labelledby="headingOne1" data-parent="#accordionEx">
          <div class="card-body">
          <p class = "text-xl-justify font-weight-normal">Este grupo foi criado com o intutio de ser pratico,simples e eficiente, para quem quer precisa de  um carro a viagens curtas.
          Temos Modelos como: HB20,FordKa,Renault Sandero, entre outros compactos .</p>
            
          </div>
        </div>

      </div>
      <div class="card">
        <div class="card-header" role="tab" id="headingTwo2">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo2"
            aria-expanded="false" aria-controls="collapseTwo2">
            <h5 class="mb-0">
              Grupo B <i class="fas fa-angle-down rotate-icon"></i>
            </h5>
          </a>
        </div>
        <div id="collapseTwo2" class="collapse" role="tabpanel" aria-labelledby="headingTwo2" data-parent="#accordionEx">
          <div class="card-body">
          Este grupo foi criado com o intutio de ser pratico,moderno ,eficiente e com grande espaço ideal para levar a familia com otimos itens de conforto para viagens  medias/longas
          Temos Modelos como: HB20S,FordKa Sedan,Renault Logan,Chevrolet Prisma entre outros Sedans .
          </div>
        </div>

      </div>
      <div class="card">
        <div class="card-header" role="tab" id="headingThree3">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree3"
            aria-expanded="false" aria-controls="collapseThree3">
            <h5 class="mb-0">
            Grupo C <i class="fas fa-angle-down rotate-icon"></i>
            </h5>
          </a>
        </div>
        <div id="collapseThree3" class="collapse" role="tabpanel" aria-labelledby="headingThree3" data-parent="#accordionEx">
          <div class="card-body">
          Este grupo foi criado com o intutio de ser Moderno ,grande espaço e robusto ideal para estradas off-road trilhas e acampamentos e Estrada Off-roads
          Temos Modelos como: Creta,Ford EcoSport,Renault Duster,Chevrolet Tracker entre outros SUVs .
          </div>
        </div>

      </div>
    </div>
      <div class="card">
        <div class="card-header" role="tab" id="headingThree4">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree4"
            aria-expanded="false" aria-controls="collapseThree4">
            <h5 class="mb-0">
            Grupo D <i class="fas fa-angle-down rotate-icon"></i>
            </h5>
          </a>
        </div>
        <div id="collapseThree4" class="collapse" role="tabpanel" aria-labelledby="headingThree4" data-parent="#accordionEx">
          <div class="card-body">
          Este grupo foi criado com o intutio de ser Moderno elegante,seguro e luxuoso ideal para viagens de negocio com o maximo conforto
          Temos Modelos como: Ford Fusion,BMWs ,Chevrolet Cruze entre outros Sedans de luxo .
          </div>
        </div>


        <div class="card">
        <div class="card-header" role="tab" id="headingThree4">
          <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree5"
            aria-expanded="false" aria-controls="collapseThree5">
            <h5 class="mb-0">
            Grupo E <i class="fas fa-angle-down rotate-icon"></i>
            </h5>
          </a>
        </div>
        <div id="collapseThree5" class="collapse" role="tabpanel" aria-labelledby="headingThree4" data-parent="#accordionEx">
          <div class="card-body">
          Este grupo foi criado com o intutio de ser Robusto,forte e seguro ideal para viagens com cargas pesadas e estradas de terra
          Temos Modelos como: Ford Ranger,Nissan Frontier ,Chevrolet S-10 entre outros PickUps .
          </div>
        </div>

      </div>