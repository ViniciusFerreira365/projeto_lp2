-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10-Mar-2019 às 15:53
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp6`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_alugacarro`
--

CREATE TABLE `tb_alugacarro` (
  `COD` int(11) NOT NULL,
  `MODELO` varchar(40) NOT NULL,
  `CAT` varchar(30) NOT NULL,
  `IMG` varchar(50) NOT NULL,
  `PRC` float NOT NULL,
  `last_modified` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_alugacarro`
--

INSERT INTO `tb_alugacarro` (`COD`, `MODELO`, `CAT`, `IMG`, `PRC`, `last_modified`) VALUES
(4, 'Hyundai HB20 1.6', 'GrupoA', 'Imagens/auto02.png', 85, '2019-03-08 10:38:16'),
(5, 'Fiat Argo 1.3', 'GrupoA', 'Imagens/auto03.png', 86, '2019-03-08 10:38:16'),
(6, 'Peugeot 208 1.2', 'GrupoA', 'Imagens/auto04.png', 83, '2019-03-08 10:38:16'),
(11, 'Ford Ka Hatch 1.0', 'GrupoA', 'Imagens/auto05.png', 81, '2019-03-08 10:38:16'),
(12, ' Chevolet Prisma 1.4 ', 'GrupoB', 'Imagens/auto09.png', 120, '2019-03-08 10:38:16'),
(13, 'Fiat Uno 1.0', 'GrupoA', 'Imagens/auto01.png', 80, '2019-03-08 10:38:16'),
(14, 'Renault Logan 1.0', 'GrupoB', 'Imagens/auto10.png', 123, '2019-03-08 10:38:16'),
(15, 'Ka Sedan 1.5', 'GrupoB', 'Imagens/auto11.png', 125, '2019-03-08 10:38:16'),
(16, 'Versa 1.6', 'GrupoB', 'Imagens/auto12.png', 130, '2019-03-08 10:38:16'),
(17, 'Hyundai HB20S 1.6', 'GrupoB', 'Imagens/auto13.png', 138, '2019-03-08 10:38:16'),
(18, 'Ford EcoSport 1.6', 'GrupoC', 'Imagens/auto16.png', 150, '2019-03-08 10:38:16'),
(19, 'Nissan Kicks 1.6', 'GrupoC', 'Imagens/auto17.png', 154, '2019-03-08 10:38:16'),
(20, 'Citroen Aircross 1.6 ', 'GrupoC', 'Imagens/auto18.png', 157, '2019-03-08 10:38:16'),
(21, 'Renault Duster 1.8', 'GrupoC', 'Imagens/auto19.png', 151, '2019-03-08 10:38:16'),
(22, 'Jeep Renegade 1.5', 'GrupoC', 'Imagens/auto20.png', 156, '2019-03-08 10:38:16'),
(24, 'Toyota Corolla GLI 1.8 ', 'GrupoD', 'Imagens/auto21.png', 165, '2019-03-08 10:38:16'),
(25, 'Chevrolet Cruze 1.8', 'GrupoD', 'Imagens/auto22.png', 168, '2019-03-08 10:38:16'),
(26, 'Ford Focus Fastback 2.0', 'GrupoD', 'Imagens/auto23.png', 162, '2019-03-08 10:38:16'),
(27, 'Citroen C4 Lounge 1.6 ', 'GrupoD', 'Imagens/auto24.png', 170, '2019-03-08 10:38:16'),
(28, 'Ford Focus Sedan 2.0', 'GrupoD', 'Imagens/auto25.png', 167, '2019-03-08 10:38:16'),
(30, 'Nissan Frontier 2.3', 'GrupoE', 'Imagens/auto26.png', 172, '2019-03-08 10:38:16'),
(31, 'Volkswagen Saveiro 2.0', 'GrupoE', 'Imagens/auto27.png', 170, '2019-03-08 10:38:16'),
(32, 'Renault Duster Oroch  2.0', 'GrupoE', 'Imagens/auto28.png', 177, '2019-03-08 10:38:16'),
(33, 'Volkswagen Amarok 2.8', 'GrupoE', 'Imagens/auto29.png', 180, '2019-03-08 10:38:16'),
(34, 'Ford F-150 3.0', 'GrupoE', 'Imagens/auto30.png', 185, '2019-03-08 10:38:16');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `sobrenome` varchar(50) NOT NULL,
  `email` varchar(80) NOT NULL,
  `descricao` varchar(256) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_alugacarro`
--
ALTER TABLE `tb_alugacarro`
  ADD PRIMARY KEY (`COD`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_alugacarro`
--
ALTER TABLE `tb_alugacarro`
  MODIFY `COD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
